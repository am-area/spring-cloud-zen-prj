package ${package};

import ${package}.model.Example;
import ${package}.repository.ExampleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;

import java.time.LocalDateTime;

@SpringBootApplication
public class Application {

    @Autowired
    private Environment environment;


    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Bean
    public CommandLineRunner initLocalRepo(ExampleRepository exampleRepository) {
        return (args) -> {
            //if(Arrays.stream(environment.getActiveProfiles()).anyMatch("local"::equals)) {
                Example example = new Example(null, "First Item", 10);

                exampleRepository.save(example);

                example = new Example(null, "Second Item", 20);

                exampleRepository.save(example);


                example = new Example(null, "Third Item", 30);

                exampleRepository.save(example);
            //}
        };
    }
}