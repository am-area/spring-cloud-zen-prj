package ${package}.business;

import ${package}.dto.ExampleDto;
import ${package}.exception.CustomException;
import ${package}.model.Example;
import ${package}.model.mapper.ExampleMapper;
import ${package}.repository.ExampleRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Slf4j
public class DefaultExampleService implements ExampleService {

    @Autowired
    private ExampleRepository exampleRepository;
    

    @Override
    public ExampleDto findExample(String exampleId) {
        Optional<Example> example = exampleRepository.findById(exampleId);

        if(example.isPresent())
            return ExampleMapper.INSTANCE.exampleToExampleDto(example.get());
        else
            return null;
    }

    @Override
    @Transactional
    public ExampleDto saveExample(ExampleDto exampleDto) {
        if(exampleDto == null)
            throw new IllegalArgumentException("example cannot be null");

        exampleDto.setCreationTime(LocalDateTime.now());
        Example example = exampleRepository.save(ExampleMapper.INSTANCE.exampleDtoToExample(exampleDto));

        exampleDto.setId(example.getId());

        //Do your business logic

        return exampleDto;
    }

    @Override
    @Transactional
    public void updateExample(ExampleDto exampleDto) {
        if(exampleDto == null)
            throw new IllegalArgumentException("example cannot be null");

        Optional<Example> exampleOpt = exampleRepository.findById(exampleDto.getId());

        if(!exampleOpt.isPresent())
            throw new IllegalArgumentException("example not found");

        Example example = exampleOpt.get();

        example.setActive(exampleDto.getActive());
        example.setUpdateTime(LocalDateTime.now());
    }

    @Override
    @Transactional
    public void removeExample(String exampleId) {
        if(StringUtils.isEmpty(exampleId))
            throw new IllegalArgumentException("example id cannot be null");

        Optional<Example> exampleOpt = exampleRepository.findById(exampleId);

        if(!exampleOpt.isPresent())
            throw new IllegalArgumentException("example not found");

        exampleRepository.delete(exampleOpt.get());
    }
    
}
