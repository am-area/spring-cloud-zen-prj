package ${package}.business;

import ${package}.dto.ExampleDto;

import java.util.List;

public interface ExampleService {

    ExampleDto findExample(String exampleId);


    ExampleDto saveExample(ExampleDto example);


    void updateExample(ExampleDto example);


    void removeExample(String exampleId);

}
