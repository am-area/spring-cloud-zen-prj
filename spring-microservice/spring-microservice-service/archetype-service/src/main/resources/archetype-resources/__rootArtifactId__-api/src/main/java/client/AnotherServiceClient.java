package ${package}.client;

import org.springframework.cloud.openfeign.FeignClient;

/**
 * This is an example of feign client
 * The power of the pattern adopted is that you can create a client to call another service
 * simply extendind the interface where APIs are defined (you can import directly)
 * Commented in order to avoid compilation problem (no anotherserviceAPI is available in this example)
 */
/*@FeignClient(value = "another-service", fallbackFactory = AnotherServiceFallbackFactory.class)
public interface AnotherServiceClient extends AnotherServiceApi {
}*/
public interface AnotherServiceClient{}