package ${package}.controller;

import it.zenproject.service.common.api.ApiOutcome;
import ${package}.ExampleApi;
import ${package}.business.ExampleService;
import ${package}.dto.ExampleDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
public class ExampleController implements ExampleApi {

    @Autowired
    private ExampleService exampleService;

    @Override
    public ApiOutcome<ExampleDto> getExample(String exampleId) {
        return new ApiOutcome(exampleService.findExample(exampleId));
    }

    @Override
    public ApiOutcome<ExampleDto> createExample(@Valid @RequestBody ExampleDto example) {
        return new ApiOutcome(exampleService.saveExample(example));
    }

    @Override
    public ApiOutcome updateExample(String exampleId, @Valid @RequestBody ExampleDto example) {
        exampleService.updateExample(example);

        return new ApiOutcome(HttpStatus.OK, "Example updated correctly");
    }

    @Override
    public ApiOutcome deleteExample(String exampleId) {
        exampleService.removeExample(exampleId);

        return new ApiOutcome(HttpStatus.OK, "Example removed correctly");
    }

}
