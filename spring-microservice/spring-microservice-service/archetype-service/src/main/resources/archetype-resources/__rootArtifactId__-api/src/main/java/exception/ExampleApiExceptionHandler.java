package ${package}.exception;

import it.zenproject.service.common.api.ApiOutcome;
import it.zenproject.service.common.exception.BaseRestExceptionHandler;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;


@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
public class ExampleApiExceptionHandler extends BaseRestExceptionHandler {

    @ExceptionHandler(CustomException.class)
    protected ResponseEntity<Object> handleFailedToDoSomething(
            CustomException ex) {
        ApiOutcome apiError = new ApiOutcome(HttpStatus.SERVICE_UNAVAILABLE, ex.getMessage());
        return buildResponseEntity(apiError);
    }
}
