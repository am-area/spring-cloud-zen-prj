package ${package}.model.mapper;

import ${package}.dto.ExampleDto;
import ${package}.model.Example;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface ExampleMapper {

    ExampleMapper INSTANCE = Mappers.getMapper(ExampleMapper.class);

    ExampleDto exampleToExampleDto(Example example);

    Example exampleDtoToExample(ExampleDto exampleDto);
}
