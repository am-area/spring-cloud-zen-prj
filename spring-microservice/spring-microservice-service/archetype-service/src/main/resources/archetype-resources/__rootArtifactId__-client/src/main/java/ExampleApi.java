package ${package};

import it.zenproject.service.common.api.ApiOutcome;
import ${package}.dto.ExampleDto;
import io.swagger.annotations.*;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * The API interface allow tu separate the definition and the controller
 * We can use the interface as FeignClient to enable microservice communication
 * without duplicate the code
 */
@Api(value = "examples", description = "just an example crud API")
@RequestMapping("/api/v1/examples")
public interface ExampleApi {


    @ApiOperation(value = "Example detail",
            nickname = "getExample",
            notes = "Retrieve example by given id",
            response = ExampleDto.class, tags = {})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Status 200", response = ExampleDto.class)})
    @GetMapping("/{exampleId}")
    ApiOutcome<ExampleDto> getExample(@ApiParam(value = "example id", required = true)
                                @PathVariable("exampleId") String exampleId);



    @ApiOperation(value = "Create Example",
            nickname = "createExample",
            notes = "Create new example",
            response = ExampleDto.class, tags = {})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Status 200", response = ExampleDto.class)})
    @RequestMapping(value = "/",
            produces = {"application/json"},
            method = RequestMethod.POST)
    ApiOutcome<ExampleDto> createExample(@Valid @RequestBody ExampleDto example);



    @ApiOperation(value = "Update Example",
            nickname = "updateExample",
            notes = "Update existing example",
            response = ApiOutcome.class, tags = {})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Status 200", response = ApiOutcome.class)})
    @RequestMapping(value = "/{exampleId}",
            produces = {"application/json"},
            method = RequestMethod.PUT)
    ApiOutcome updateExample(@ApiParam(value = "example id", required = true)
                          @PathVariable("exampleId") String exampleId,
                          @Valid @RequestBody ExampleDto example);



    @ApiOperation(value = "Delete Example",
            nickname = "deleteExample",
            notes = "Delete existing example",
            response = ApiOutcome.class, tags = {})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Status 200", response = ApiOutcome.class)})
    @RequestMapping(value = "/{exampleId}",
            produces = {"application/json"},
            method = RequestMethod.DELETE)
    ApiOutcome deleteExample(@ApiParam(value = "example id", required = true)
                          @PathVariable("exampleId") String exampleId);



}
