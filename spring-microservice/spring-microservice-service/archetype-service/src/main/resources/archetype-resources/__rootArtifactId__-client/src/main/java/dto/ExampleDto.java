package ${package}.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;

import it.zenproject.service.common.model.BaseModel;

/**
 * Just an example to understand archetype structure
 * This is a DTO object designed for data transfer
 * In zen project we use mapstruct to map model with dto
 * Lombock is used to avoid getter and setter explicit declaration
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ExampleDto extends BaseModel {

    @NotNull
    private String id;

    private String name;

    private Integer age;

}
