package it.zenproject.service.common.model;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public abstract class BaseModel {

    protected LocalDateTime creationTime;

    protected LocalDateTime updateTime;

    //Every model should allow logic deletion
    protected Boolean active;
}
