package it.zenproject.service.notification.api.business;

import it.zenproject.service.common.api.ApiOutcome;
import it.zenproject.service.notification.api.client.UserClient;
import it.zenproject.service.user.api.dto.UserDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DefaultUserService implements UserService {
    @Autowired
    private UserClient userClient;

    @Override
    public UserDto getUserInfo(Long userId) {
        ApiOutcome<UserDto> user = userClient.getUser(userId);
        if(user == null)
            return null;

        return user.getBody();
    }
}
