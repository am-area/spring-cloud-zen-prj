package it.zenproject.service.notification.api.business;

import it.zenproject.service.notification.api.dto.NotificationDto;

public interface NotificationService {

    void sendNotification(NotificationDto notification);

}
