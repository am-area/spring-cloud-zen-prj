package it.zenproject.service.notification.api.business;

import it.zenproject.service.notification.api.dto.NotificationDto;
import it.zenproject.service.user.api.dto.UserDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class SmsNotificationService implements NotificationService {

    @Autowired
    private UserService userService;

    @Override
    public void sendNotification(NotificationDto notification) {
        UserDto userInfo = userService.getUserInfo(notification.getUserId());

        if(userInfo == null)
            throw new IllegalArgumentException("User not found");

        log.info("[APP] I'm sending an SMS to {}", userInfo.getTelephone());

    }

}
