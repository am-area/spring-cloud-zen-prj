package it.zenproject.service.notification.api.business;

import it.zenproject.service.user.api.dto.UserDto;

public interface UserService {

    UserDto getUserInfo(Long userId);

}
