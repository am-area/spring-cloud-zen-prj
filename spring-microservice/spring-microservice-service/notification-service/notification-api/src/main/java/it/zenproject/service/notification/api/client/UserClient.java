package it.zenproject.service.notification.api.client;

import it.zenproject.service.user.api.UserApi;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(value = "user-service", fallbackFactory = UserFallbackFactory.class)
public interface UserClient extends UserApi {
}
