package it.zenproject.service.notification.api.client;

import it.zenproject.service.common.api.ApiOutcome;
import it.zenproject.service.user.api.dto.UserDto;
import feign.hystrix.FallbackFactory;
import org.springframework.stereotype.Component;

import javax.validation.Valid;

@Component
public class UserFallbackFactory implements FallbackFactory<UserClient> {

    @Override
    public UserClient create(Throwable throwable) {
        return new UserClient() {
            @Override
            public ApiOutcome<UserDto> getUser(Long userId) {
                return null;
            }

            @Override
            public ApiOutcome<UserDto> createUser(@Valid UserDto user) {
                return null;
            }

            @Override
            public ApiOutcome updateUser(Long userId, @Valid UserDto user) {
                return null;
            }

            @Override
            public ApiOutcome deleteUser(Long userId) {
                return null;
            }
        };
    }
}
