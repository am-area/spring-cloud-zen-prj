package it.zenproject.service.notification.api.controller;

import it.zenproject.service.common.api.ApiOutcome;
import it.zenproject.service.notification.api.NotificationApi;
import it.zenproject.service.notification.api.business.NotificationService;
import it.zenproject.service.notification.api.dto.NotificationDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
public class NotificationController implements NotificationApi {

    @Autowired
    @Qualifier("emailNotificationService")
    private NotificationService emailNotificationService;

    @Autowired
    @Qualifier("smsNotificationService")
    private NotificationService smsNotificationService;

    @Autowired
    @Qualifier("pushNotificationService")
    private NotificationService pushNotificationService;


    @Override
    public ApiOutcome sendNotification(@Valid @RequestBody NotificationDto notification) {

        switch (notification.getChannel()) {
            case EMAIL:
                emailNotificationService.sendNotification(notification);
                break;
            case SMS:
                smsNotificationService.sendNotification(notification);
                break;
            case PUSH:
                pushNotificationService.sendNotification(notification);
                break;
        }

        return new ApiOutcome(HttpStatus.OK, "Notification sent successfully");
    }


}
