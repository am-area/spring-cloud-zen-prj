package it.zenproject.service.notification.api;

import it.zenproject.service.common.api.ApiOutcome;
import it.zenproject.service.notification.api.dto.NotificationDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;


@Api(value = "notifications", description = "the notifications API")
@RequestMapping("/api/v1/notifications")
public interface NotificationApi {


    @ApiOperation(value = "Send Notification",
            nickname = "sendNotification",
            notes = "Send new notification",
            response = ApiOutcome.class, tags = {})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Status 200", response = ApiOutcome.class)})
    @RequestMapping(value = "/send",
            produces = {"application/json"},
            method = RequestMethod.POST)
    ApiOutcome sendNotification(@Valid @RequestBody NotificationDto notification);



}
