package it.zenproject.service.notification.api.dto;

public enum ChannelDto {
    EMAIL,
    SMS,
    PUSH
}
