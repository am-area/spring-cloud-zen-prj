package it.zenproject.service.notification.api.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class NotificationDto {

    @NotNull
    private String taskId;

    @NotNull
    private String taskDescription;

    @NotNull
    private Long userId;

    @NotNull
    private ChannelDto channel;

}
