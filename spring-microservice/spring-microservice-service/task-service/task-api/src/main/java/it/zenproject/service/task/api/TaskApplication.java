package it.zenproject.service.task.api;

import it.zenproject.service.task.api.model.Task;
import it.zenproject.service.task.api.repository.TaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;

import java.time.LocalDateTime;

@SpringBootApplication
public class TaskApplication {

    @Autowired
    private Environment environment;


    public static void main(String[] args) {
        SpringApplication.run(TaskApplication.class, args);
    }

    @Bean
    public CommandLineRunner initLocalRepo(TaskRepository taskRepository) {
        return (args) -> {
            //if(Arrays.stream(environment.getActiveProfiles()).anyMatch("local"::equals)) {
                Task task = new Task(null, "Have breakfast", LocalDateTime.now(), null, true, 1L);

                taskRepository.save(task);

                task = new Task(null, "Have lunch", LocalDateTime.now(), null, true, 1L);

                taskRepository.save(task);


                task = new Task(null, "Have dinner", LocalDateTime.now(), null, true, 1L);

                taskRepository.save(task);
            //}
        };
    }
}