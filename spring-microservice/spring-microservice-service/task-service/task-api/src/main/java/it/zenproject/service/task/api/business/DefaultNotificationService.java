package it.zenproject.service.task.api.business;

import it.zenproject.service.common.api.ApiOutcome;
import it.zenproject.service.notification.api.dto.NotificationDto;
import it.zenproject.service.task.api.client.NotificationClient;
import it.zenproject.service.task.api.exception.SendNotificationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service
public class DefaultNotificationService implements NotificationService {

    @Autowired
    private NotificationClient notificationClient;

    @Override
    public void sendNotification(NotificationDto notificationDto) throws SendNotificationException {
        ApiOutcome outcome =
                notificationClient.sendNotification(notificationDto);

        if(!outcome.getStatus().equals(HttpStatus.OK))
            throw new SendNotificationException(outcome.getMessage());
    }
}
