package it.zenproject.service.task.api.business;

import it.zenproject.service.notification.api.dto.ChannelDto;
import it.zenproject.service.notification.api.dto.NotificationDto;
import it.zenproject.service.task.api.dto.TaskDto;
import it.zenproject.service.task.api.exception.SendNotificationException;
import it.zenproject.service.task.api.model.Task;
import it.zenproject.service.task.api.model.mapper.TaskMapper;
import it.zenproject.service.task.api.repository.TaskRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Slf4j
public class DefaultTaskService implements TaskService {

    @Autowired
    private TaskRepository taskRepository;

    @Autowired
    private NotificationService notificationService;

    @Override
    public TaskDto findTask(String taskId) {
        Optional<Task> task = taskRepository.findById(taskId);

        if(task.isPresent())
            return TaskMapper.INSTANCE.taskToTaskDto(task.get());
        else
            return null;
    }

    @Override
    @Transactional
    public TaskDto saveTask(TaskDto taskDto) {
        if(taskDto == null)
            throw new IllegalArgumentException("task cannot be null");

        taskDto.setCreationDate(LocalDateTime.now());
        Task task = taskRepository.save(TaskMapper.INSTANCE.taskDtoToTask(taskDto));

        taskDto.setId(task.getId());

        //Send notification (optional)
        try {
            notificationService.sendNotification(
                    new NotificationDto(taskDto.getId(),
                    taskDto.getDescription(),
                    taskDto.getUserId(),
                    ChannelDto.EMAIL));

            notificationService.sendNotification(
                    new NotificationDto(taskDto.getId(),
                            taskDto.getDescription(),
                            taskDto.getUserId(),
                            ChannelDto.SMS));

            notificationService.sendNotification(
                    new NotificationDto(taskDto.getId(),
                            taskDto.getDescription(),
                            taskDto.getUserId(),
                            ChannelDto.PUSH));
        } catch (SendNotificationException e) {
            log.error("[APP] Failed to send notification due to: {}", e.getMessage());
            throw e;
        }

        return taskDto;
    }

    @Override
    @Transactional
    public void updateTask(TaskDto taskDto) {
        if(taskDto == null)
            throw new IllegalArgumentException("task cannot be null");

        Optional<Task> taskOpt = taskRepository.findById(taskDto.getId());

        if(!taskOpt.isPresent())
            throw new IllegalArgumentException("task not found");

        Task task = taskOpt.get();

        task.setActive(taskDto.getActive());
        task.setUpdateTime(LocalDateTime.now());
        task.setDescription(taskDto.getDescription());
    }

    @Override
    @Transactional
    public void removeTask(String taskId) {
        if(StringUtils.isEmpty(taskId))
            throw new IllegalArgumentException("task id cannot be null");

        Optional<Task> taskOpt = taskRepository.findById(taskId);

        if(!taskOpt.isPresent())
            throw new IllegalArgumentException("task not found");

        taskRepository.delete(taskOpt.get());
    }

    @Override
    public List<TaskDto> findTaskByUserId(Long idUser) {
        List<Task> taskList = taskRepository.findByUserId(idUser);

        if(taskList == null || taskList.isEmpty())
            return new ArrayList<>();

        log.info("Found {} tasks", taskList.size());

        return taskList.stream().map(
                tsk -> TaskMapper.INSTANCE.taskToTaskDto(tsk))
                .collect(Collectors.toList());
    }
}
