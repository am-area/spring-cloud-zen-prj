package it.zenproject.service.task.api.business;

import it.zenproject.service.notification.api.dto.NotificationDto;
import it.zenproject.service.task.api.exception.SendNotificationException;

public interface NotificationService {

    void sendNotification(NotificationDto notificationDto) throws SendNotificationException;
}
