package it.zenproject.service.task.api.business;

import it.zenproject.service.task.api.dto.TaskDto;

import java.util.List;

public interface TaskService {

    TaskDto findTask(String taskId);


    TaskDto saveTask(TaskDto task);


    void updateTask(TaskDto task);


    void removeTask(String taskId);


    List<TaskDto> findTaskByUserId(Long idUser);

}
