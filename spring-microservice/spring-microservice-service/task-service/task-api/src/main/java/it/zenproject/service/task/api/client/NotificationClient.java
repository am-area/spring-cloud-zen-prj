package it.zenproject.service.task.api.client;

import it.zenproject.service.notification.api.NotificationApi;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(value = "notification-service", fallbackFactory = NotificationFallbackFactory.class)
public interface NotificationClient extends NotificationApi {
}
