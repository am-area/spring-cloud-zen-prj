package it.zenproject.service.task.api.client;

import it.zenproject.service.common.api.ApiOutcome;
import it.zenproject.service.notification.api.dto.NotificationDto;
import feign.hystrix.FallbackFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

@Component
public class NotificationFallbackFactory implements FallbackFactory<NotificationClient> {


    @Override
    public NotificationClient create(Throwable throwable) {
        return new NotificationClient() {
            @Override
            public ApiOutcome sendNotification(NotificationDto notification) {
                return new ApiOutcome<>(HttpStatus.INTERNAL_SERVER_ERROR, throwable.getMessage());
            }
        };
    }
}
