package it.zenproject.service.task.api.controller;

import it.zenproject.service.common.api.ApiOutcome;
import it.zenproject.service.task.api.TaskApi;
import it.zenproject.service.task.api.business.TaskService;
import it.zenproject.service.task.api.dto.TaskDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
@Slf4j
public class TaskController implements TaskApi {

    @Autowired
    private TaskService taskService;

    @Override
    public ApiOutcome<TaskDto> getTask(String taskId) {
        return new ApiOutcome(taskService.findTask(taskId));
    }

    @Override
    public ApiOutcome<TaskDto> createTask(@Valid @RequestBody TaskDto task) {
        return new ApiOutcome(taskService.saveTask(task));
    }

    @Override
    public ApiOutcome updateTask(String taskId, @Valid @RequestBody TaskDto task) {
        taskService.updateTask(task);

        return new ApiOutcome(HttpStatus.OK, "Task updated correctly");
    }

    @Override
    public ApiOutcome deleteTask(String taskId) {
        taskService.removeTask(taskId);

        return new ApiOutcome(HttpStatus.OK, "Task removed correctly");
    }

    @Override
    public ApiOutcome<List<TaskDto>> findTaskByUserId(Long idUser) {
        log.info("Looking for user {}", idUser);

        return new ApiOutcome(taskService.findTaskByUserId(idUser));
    }
}
