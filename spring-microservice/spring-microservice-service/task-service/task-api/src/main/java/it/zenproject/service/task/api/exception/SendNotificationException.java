package it.zenproject.service.task.api.exception;

public class SendNotificationException extends RuntimeException {
    public SendNotificationException(String errorMessage) {
        super(errorMessage);
    }
}
