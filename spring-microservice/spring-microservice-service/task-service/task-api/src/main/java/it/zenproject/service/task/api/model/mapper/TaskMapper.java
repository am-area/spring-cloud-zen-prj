package it.zenproject.service.task.api.model.mapper;

import it.zenproject.service.task.api.dto.TaskDto;
import it.zenproject.service.task.api.model.Task;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface TaskMapper {

    TaskMapper INSTANCE = Mappers.getMapper(TaskMapper.class);

    TaskDto taskToTaskDto(Task task);

    Task taskDtoToTask(TaskDto taskDto);
}
