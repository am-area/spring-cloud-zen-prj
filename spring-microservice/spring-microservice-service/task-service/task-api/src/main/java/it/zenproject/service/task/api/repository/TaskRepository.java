package it.zenproject.service.task.api.repository;

import it.zenproject.service.task.api.model.Task;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TaskRepository extends CrudRepository<Task, String> {

    List<Task> findByUserId(Long userId);
}
