package it.zenproject.service.task.api;

import it.zenproject.service.common.api.ApiOutcome;
import it.zenproject.service.task.api.dto.TaskDto;
import io.swagger.annotations.*;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;


@Api(value = "tasks", description = "the tasks API")
@RequestMapping("/api/v1/tasks")
public interface TaskApi {

    @ApiOperation(value = "Task detail",
            nickname = "getTask",
            notes = "Retrieve task by given id",
            response = TaskDto.class, tags = {})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Status 200", response = TaskDto.class)})
    @RequestMapping(value = "/{taskId}",
            produces = {"application/json"},
            method = RequestMethod.GET)
    ApiOutcome<TaskDto> getTask(@ApiParam(value = "task id", required = true)
                                    @PathVariable("taskId") String taskId);



    @ApiOperation(value = "Create Task",
            nickname = "createTask",
            notes = "Create new task",
            response = TaskDto.class, tags = {})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Status 200", response = TaskDto.class)})
    @PostMapping
    ApiOutcome<TaskDto> createTask(@Valid @RequestBody TaskDto task);



    @ApiOperation(value = "Update Task",
            nickname = "updateTask",
            notes = "Update existing task",
            response = ApiOutcome.class, tags = {})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Status 200", response = ApiOutcome.class)})
    @RequestMapping(value = "/{taskId}",
            produces = {"application/json"},
            method = RequestMethod.PUT)
    ApiOutcome updateTask(@ApiParam(value = "task id", required = true)
                                       @PathVariable("taskId") String taskId,
                                       @Valid @RequestBody TaskDto task);



    @ApiOperation(value = "Delete Task",
            nickname = "deleteTask",
            notes = "Delete existing task",
            response = ApiOutcome.class, tags = {})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Status 200", response = ApiOutcome.class)})
    @RequestMapping(value = "/{taskId}",
            produces = {"application/json"},
            method = RequestMethod.DELETE)
    ApiOutcome deleteTask(@ApiParam(value = "task id", required = true)
                                       @PathVariable("taskId") String taskId);



    @ApiOperation(value = "List of user tasks", nickname = "taskByUserId",
            notes = "List of user tasks by user identifier",
            response = TaskDto.class, responseContainer = "List", tags = {})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Status 200", response = TaskDto.class, responseContainer = "List")})
    @RequestMapping(value = "/user-id/{userId}",
            produces = {"application/json"},
            method = RequestMethod.POST)
    ApiOutcome<List<TaskDto>> findTaskByUserId(@ApiParam(value = "user identifier", required = true, example = "1")
                                                   @PathVariable("userId") Long idUser);
}
