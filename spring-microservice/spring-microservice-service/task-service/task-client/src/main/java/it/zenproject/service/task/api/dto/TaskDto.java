package it.zenproject.service.task.api.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class TaskDto {

    private String id;

    @NotNull
    @Size(max = 400)
    private String description;

    private LocalDateTime creationDate;

    private LocalDateTime updateTime;

    private Boolean active;

    @NotNull
    @Min(1)
    private Long userId;
}
