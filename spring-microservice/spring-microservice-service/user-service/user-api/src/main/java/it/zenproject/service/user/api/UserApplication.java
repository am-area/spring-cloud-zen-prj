package it.zenproject.service.user.api;

import it.zenproject.service.user.api.model.User;
import it.zenproject.service.user.api.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;

import java.time.LocalDateTime;

@SpringBootApplication
public class UserApplication {

    @Autowired
    private Environment environment;

    public static void main(String[] args) {
        SpringApplication.run(UserApplication.class, args);
    }


    @Bean
    public CommandLineRunner initLocalRepo(UserRepository userRepository) {
        return (args) -> {
            //if(Arrays.stream(environment.getActiveProfiles()).anyMatch("local"::equals)) {
                User user = new User(1L, "Alberto", "Rondi", "a.rondi@gmail.com",
                        "+393334598999", LocalDateTime.now(), null, true);

                userRepository.save(user);

                user = new User(2L, "Mattia", "Cremisi", "m.cremisi@gmail.com",
                        "+393334598998", LocalDateTime.now(), null, true);

                userRepository.save(user);


                user = new User(3L, "Lorenzo", "Barba", "l.barba@gmail.com",
                        "+393334598569", LocalDateTime.now(), null, true);

                userRepository.save(user);
            //}
        };
    }
}