package it.zenproject.service.user.api.business;

import it.zenproject.service.common.exception.ElementNotFoundException;
import it.zenproject.service.user.api.dto.UserDto;
import it.zenproject.service.user.api.model.User;
import it.zenproject.service.user.api.model.mapper.UserMapper;
import it.zenproject.service.user.api.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.time.LocalDateTime;
import java.util.Optional;

@Service
public class DefaultUserService implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDto findUser(Long userId) {
        Optional<User> user = userRepository.findById(userId);

        if(user.isPresent())
            return UserMapper.INSTANCE.userToUserDto(user.get());
        else
            throw new ElementNotFoundException(User.class, "id", userId.toString());
    }

    @Override
    @Transactional
    public UserDto saveUser(UserDto userDto) {
        if(userDto == null)
            throw new IllegalArgumentException("user cannot be null");

        User user = userRepository.save(UserMapper.INSTANCE.userDtoToUser(userDto));
        user.setCreationDate(LocalDateTime.now());

        userDto.setId(user.getId());

        return userDto;
    }

    @Override
    @Transactional
    public void updateUser(UserDto userDto) {
        if(userDto == null)
            throw new IllegalArgumentException("user cannot be null");

        Optional<User> userOpt = userRepository.findById(userDto.getId());

        if(!userOpt.isPresent())
            throw new IllegalArgumentException("user not found");

        User user = userOpt.get();

        user.setActive(userDto.getActive());
        user.setUpdateTime(LocalDateTime.now());
        user.setName(userDto.getName());
        user.setSurname(userDto.getSurname());
        user.setEmail(userDto.getEmail());
        user.setTelephone(userDto.getTelephone());
    }

    @Override
    @Transactional
    public void removeUser(Long userId) {
        if(StringUtils.isEmpty(userId))
            throw new IllegalArgumentException("user id cannot be null");

        Optional<User> userOpt = userRepository.findById(userId);

        if(!userOpt.isPresent())
            throw new IllegalArgumentException("user not found");

        userRepository.delete(userOpt.get());
    }

}
