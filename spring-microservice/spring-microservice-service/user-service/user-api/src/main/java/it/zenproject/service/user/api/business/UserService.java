package it.zenproject.service.user.api.business;

import it.zenproject.service.user.api.dto.UserDto;

public interface UserService {

    UserDto findUser(Long userId);


    UserDto saveUser(UserDto user);


    void updateUser(UserDto user);


    void removeUser(Long userId);

}
