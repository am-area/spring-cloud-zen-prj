package it.zenproject.service.user.api.controller;

import it.zenproject.service.common.api.ApiOutcome;
import it.zenproject.service.user.api.UserApi;
import it.zenproject.service.user.api.business.UserService;
import it.zenproject.service.user.api.dto.UserDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
public class UserController implements UserApi {

    @Autowired
    private UserService userService;

    @Override
    public ApiOutcome<UserDto> getUser(Long userId) {
        return new ApiOutcome<>(userService.findUser(userId));
    }

    @Override
    public ApiOutcome<UserDto> createUser(@Valid @RequestBody UserDto user) {
        return new ApiOutcome<>(userService.saveUser(user));
    }

    @Override
    public ApiOutcome<UserDto> updateUser(Long userId, @Valid @RequestBody UserDto user) {
        userService.updateUser(user);

        return new ApiOutcome(HttpStatus.OK, "User updated correctly");
    }

    @Override
    public ApiOutcome<UserDto> deleteUser(Long userId) {
        userService.removeUser(userId);

        return new ApiOutcome(HttpStatus.OK, "User removed correctly");
    }

}
