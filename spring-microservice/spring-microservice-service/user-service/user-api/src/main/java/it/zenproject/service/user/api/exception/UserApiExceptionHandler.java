package it.zenproject.service.user.api.exception;

import it.zenproject.service.common.exception.BaseRestExceptionHandler;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.web.bind.annotation.ControllerAdvice;

@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
public class UserApiExceptionHandler extends BaseRestExceptionHandler {

}
