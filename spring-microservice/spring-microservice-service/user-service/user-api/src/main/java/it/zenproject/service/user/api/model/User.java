package it.zenproject.service.user.api.model;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(of = "id")
public class User {

    @Id
    private Long id;

    private String name;

    private String surname;

    private String email;

    private String telephone;

    private LocalDateTime creationDate;

    private LocalDateTime updateTime;

    private Boolean active;
}
