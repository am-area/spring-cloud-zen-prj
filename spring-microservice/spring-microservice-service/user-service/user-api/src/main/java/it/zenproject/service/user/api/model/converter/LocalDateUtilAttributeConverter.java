package it.zenproject.service.user.api.model.converter;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

@Converter(autoApply = true)
public class LocalDateUtilAttributeConverter implements AttributeConverter<LocalDate, Date> {

    @Override
    public Date convertToDatabaseColumn(LocalDate date) {
        if(date == null)
            return null;

        //date.now().atStartOfDay().toInstant(ZoneOffset.UTC);
        //Instant instant = Instant.from(date);
        //return Date.from(instant);
        return Date.from(date.atStartOfDay(ZoneId.systemDefault()).toInstant());
    }

    @Override
    public LocalDate convertToEntityAttribute(Date value) {
        if(value == null)
            return null;

        //Instant instant = value.toInstant();
        //return LocalDate.from(instant);
        LocalDate date = value.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        return date;
    }
}
