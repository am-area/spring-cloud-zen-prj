package it.zenproject.service.user.api.model.mapper;

import it.zenproject.service.user.api.dto.UserDto;
import it.zenproject.service.user.api.model.User;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface UserMapper {

    UserMapper INSTANCE = Mappers.getMapper(UserMapper.class);

    UserDto userToUserDto(User user);

    User userDtoToUser(UserDto userDto);
}
