package it.zenproject.service.user.api;

import it.zenproject.service.common.api.ApiOutcome;
import it.zenproject.service.user.api.dto.UserDto;
import io.swagger.annotations.*;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;


@Api(value = "users")
@RequestMapping("/api/v1/users")
public interface UserApi {

    @ApiOperation(value = "User detail",
            nickname = "getUser",
            notes = "Retrieve user by given id",
            response = UserDto.class, tags = {})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Status 200", response = UserDto.class)})
    @RequestMapping(value = "/{userId}",
            produces = {"application/json"},
            method = RequestMethod.GET)
    ApiOutcome<UserDto> getUser(@ApiParam(value = "user id", required = true, example = "1")
                                    @PathVariable("userId") Long userId);



    @ApiOperation(value = "Create User",
            nickname = "createUser",
            notes = "Create new user",
            response = UserDto.class, tags = {})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Status 200", response = UserDto.class)})
    @RequestMapping(value = "/",
            produces = {"application/json"},
            method = RequestMethod.POST)
    ApiOutcome<UserDto> createUser(@Valid @RequestBody UserDto user);



    @ApiOperation(value = "Update User",
            nickname = "updateUser",
            notes = "Update existing user",
            response = ApiOutcome.class, tags = {})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Status 200", response = ApiOutcome.class)})
    @RequestMapping(value = "/{userId}",
            produces = {"application/json"},
            method = RequestMethod.PUT)
    ApiOutcome updateUser(@ApiParam(value = "user id", required = true, example = "123")
                                       @PathVariable("userId") Long userId,
                                       @Valid @RequestBody UserDto user);



    @ApiOperation(value = "Delete User",
            nickname = "deleteUser",
            notes = "Delete existing user",
            response = ApiOutcome.class, tags = {})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Status 200", response = ApiOutcome.class)})
    @RequestMapping(value = "/{userId}",
            produces = {"application/json"},
            method = RequestMethod.DELETE)
    ApiOutcome deleteUser(@ApiParam(value = "user id", required = true, example = "123")
                                       @PathVariable("userId") Long userId);


}
